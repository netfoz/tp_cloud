# TP1 : Programmatic provisioning

🌞 **Ajustez le `Vagrantfile` pour que la VM créée** :

- ait l'IP `10.1.1.11/24`
- porte le hostname `ezconf.tp1.efrei`
- porte le nom (pour Vagrant) `ezconf.tp1.efrei` (ce n'est pas le hostname de la machine)
- ait 2G de RAM
- ait un disque dur de 20G

> **Vagrantfile :**

```
Vagrant.configure("2") do |config|

  config.vm.box = "generic/rocky9"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.define "ezconf.tp1.efrei"
  config.vm.network :private_network, ip: "10.1.1.11", netmask: "255.255.255.0"
  config.vm.disk :disk, name: "backup", size: "20GB"
  
  config.vm.provider :virtualbox do |v| 

    v.memory = 2048

  end 

end
```

# II. Initialization script

🌞 **Ajustez le `Vagrantfile`** :

- quand la VM démarre, elle doit exécuter un script bash
- le script installe les paquets `vim` et `python3`
- il met aussi à jour le système avec un `dnf update -y` (si c'est trop long avec le réseau de l'école, zappez cette étape)

> **script.sh :**

```
#!/bin/bash

sudo dnf install -y vim python3
```

> **Modification du Vagrantfile :**

```
Vagrant.configure("2") do |config|

  config.vm.box = "generic/rocky9"
  config.vm.hostname = "ezconf.tp1.efrei"
  config.vm.define "ezconf.tp1.efrei"
  config.vm.network :private_network, ip: "10.1.1.11", netmask: "255.255.255.0"
  config.vm.disk :disk, name: "backup", size: "20GB"

  config.vm.provision "shell", path: "script.sh"
  
  config.vm.provider :virtualbox do |v| 

    v.memory = 2048

  end 

end
```

# III. Repackaging

🌞 **Repackager la VM créée précédemment**

> **Commandes utilisées :**

```
$ vagrant package --output cloud-one.box

$ vagrant box add cloud-one cloud-one.box

$ vagrant box list
cloud-one      (virtualbox, 0)
generic/rocky9 (virtualbox, 4.3.12, (amd64))

$vagrant box repackage cloud-one virtualbox 0
```

# IV. Multi VM

🌞 **Un deuxième `Vagrantfile` qui définit** :

- une VM `node1.tp1.efrei`
  - IP `10.1.1.101/24`
  - 2G de RAM
- une VM `node2.tp1.efrei`
  - IP `10.1.1.102/24`
  - 1G de RAM

> **Vagrantfile :**

```
Vagrant.configure("2") do |config|
  config.vm.box = "generic/rocky9"
  
  vms = [
    { name: "node1.tp1.efrei", ip: "10.1.1.101", memory: "2048" },
    { name: "node2.tp1.efrei", ip: "10.1.1.102", memory: "1024" }
  ]

  vms.each do |vm|
    config.vm.define vm[:name] do |node|
      node.vm.hostname = vm[:name]
      node.vm.network :private_network, ip: vm[:ip], netmask: "255.255.255.0"
      node.vm.provider :virtualbox do |vb|
        vb.memory = vm[:memory]
      end
    end
  end
end
```

🌞 **Une fois les VMs allumées, assurez-vous que vous pouvez ping `10.1.1.102` depuis `node1`**

```
$ vagrant status
Current machine states:

node1.tp1.efrei           running (virtualbox)
node2.tp1.efrei           running (virtualbox)
```
```
$ vagrant ssh node1.tp1.efrei
[vagrant@node1 ~]$ ping 10.1.1.102
PING 10.1.1.102 (10.1.1.102) 56(84) bytes of data.
64 bytes from 10.1.1.102: icmp_seq=1 ttl=64 time=34.6 ms
64 bytes from 10.1.1.102: icmp_seq=2 ttl=64 time=1.29 ms
64 bytes from 10.1.1.102: icmp_seq=3 ttl=64 time=4.74 ms
```

# V. cloud-init

## 1. Repackaging

🌞 **Repackager une box Vagrant**

- cette box doit contenir le paquet `cloud-init` pré-installé
- il faut aussi avoir saisi `systemctl enable cloud-init` après avoir l'instalaltion du paquet
  - cela permet à `cloud-init` de démarrer automatiquement au prochain boot de la machine

> **Vagrantfile :**

```
Vagrant.configure("2") do |config|
  config.vm.box = "cloud-one"
end
```

**Connexion à la box :**

```
$ vagrant ssh
```

**Installation de cloud-init dans la box :**

```
[vagrant@rocky9 ~]$ sudo dnf install cloud-init
```

**Installation du paquet :**

```
[vagrant@rocky9 ~]$ sudo systemctl enable cloud-init
```

## 2. Test

🌞 **Tester !**

- écrire un `Vagrantfile` qui utilise la box repackagée
- allumez la VM avec `vagrant up` et vérifiez que `cloud-init` a bien créé l'utilisateur, avec le bon password, et la bonne clé SSH

> **Vagrantfile :**

```
Vagrant.configure("2") do |config|
  config.vm.box = "cloud-one"
  config.vm.disk :dvd, name:"installer", file: "./cloud-init.iso"
end
```

> **Création du USER par cloud-init :**

```
lala:x:1001:1001:Super adminsys:/home/lala:/bin/bash
```

> **Création du PASSWORD par cloud-init :**

lala:$6$bV62paDqH/ZQSVFb$jiBgcgpkuzmmoZSvvLPwpd4gjwvnKQEWTE119tMNTnICtMcJ6dyPcDCVaTur8j5UQFuxAAM6eTimGdr97Nagh1:19822:0:99999:7:::

> **Création de la clé SSH par cloud-init :**

```
ssh-ed25519 AAAAC3NzaC1lRU51NTE5AAAAIMO/JQ3AtA3k8iXJWlkdUKSHDh215OKyLR0vauzD7BgA
```